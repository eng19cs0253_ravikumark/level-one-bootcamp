//Write a program to find the volume of a tromboloid using one function

#include <stdio.h>

int main()
{
     int h,d,b;
     printf("Enter the value for h : ");
     scanf("%d", &h);
     printf("Enter the value for d: ");
     scanf("%d", &d);
     printf("Enter the value for b greater than zero : ");
     scanf("%d", &b);   
     double volume =  (1.0/3.0) * ((h * d *  b) + ( d / b)); //volume of tromboloid
     printf("Volume : %.2f", volume);
     return 0;
}


