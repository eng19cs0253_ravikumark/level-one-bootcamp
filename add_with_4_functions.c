//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>

int input()
{
    int a; 
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

int Add_two_nos(int *a, int *b)
{
    int sum;
    sum = *a + *b;
    return sum;
}

void output(int *a, int *b, int z)
{
    printf("The Sum of %d and %d is %d\n",*a,* b,z);
}

int main()
{
    int x,y,z;
    x = input();
    y = input();
    z = Add_two_nos(&x,&y);
    output(&x,&y,z);
    return 0;
}
