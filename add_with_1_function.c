//Write a program to add two user input numbers using one function.

#include<stdio.h>

int main()
{
   int a,b;
   printf("Enter the values for a and b\n ");
   scanf("%d %d",&a,&b);
   int sum=a+b;  // Adds two numbers
   printf("The sum of %d and %d is %d\n",a,b,sum);
   return 0;
}

