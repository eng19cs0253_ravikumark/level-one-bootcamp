//WAP to find the sum of two fractions.

#include<stdio.h>

struct fract
{
  int nume;
  int deno;
};

typedef struct fract Fract;
Fract input()
{
   Fract r;
   scanf("%d",&r.nume);
   scanf("%d",&r.deno);
   return r;
}

int gcd(int a, int b)
{
  int i,x;
  for(i=2; i<=a && i<=b;i++)
  {
    if(a%i==0 && b%i==0)
    x=i;
  }
  return x;
}

Fract cal_sum(Fract r1,Fract r2)
{
  Fract p;
  int GCD;
  p.nume= (r1.nume*r2.deno)+(r2.nume*r1.deno);
  p.deno=(r1.deno*r2.deno);
  GCD=gcd(p.nume,p.deno);
  p.nume=p.nume/GCD;
  p.deno=p.deno/GCD;
  return p;
}

void output(Fract r1,Fract r2,Fract result)
{
  printf("The sum of %d/%d and %d/%d is %d/%d",r1.nume,r1.deno,r2.nume,r2.deno, result.nume,result.deno);
  printf("\n");
}

int main()
{
  Fract r1,r2,result;
  printf("Enter the first fraction:\n");
  r1=input();
  printf("Enter the second fraction:\n");
  r2=input();
  result = cal_sum(r1,r2);
  output(r1,r2,result);
  return 0;
}
