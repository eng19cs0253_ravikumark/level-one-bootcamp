//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>

int input()
{
    float x;
    scanf("%f",&x);
    return x;
}


float vol_tromboloid(float h, float d,float b)
{
    double volume =  (1.0/3.0) * ((h * d *  b) + ( d / b)); //volume of tromboloid
    return volume;
}


void output(double vol)
{
    printf("The Volume of tromboloid is %.2f\n",vol);
}


int main()
{
    float h,d,b;
    printf("Enter the value for h:\n");
    h=input();
    printf("Enter the value for d:\n");
    d=input();
    printf("Enter the value for b greater then zero:\n");
    b=input();
    double volume = vol_tromboloid(h,b,d);
    output(volume);
    return 0;
}
